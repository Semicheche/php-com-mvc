<?php

namespace App\Controllers;

use SON\Controller\Action;

class indexController extends Action
{

  public function index()
  {
    $this->view->cars = array('Mustang','Ferrari', 'Bugatti' );
    $this->render("index");
  }

  public function contact()
  {
    $this->view->contact = array('Jose','Maria', 'João');
    $this->render("contact",false);
  }

}
?>
