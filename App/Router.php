<?php

namespace App;

use SON\Init\Bootstrap;

class Router extends Bootstrap
{

    protected function initRoutes()
    {
        $routes['home'] = array('route'=>'/', 'controller'=>"indexController", 'action'=>'index');
        $routes['contact'] = array('route'=>'/contact', 'controller'=>"indexController", 'action'=>'contact');
        $this->setRouters($routes);
    }

}

?>
